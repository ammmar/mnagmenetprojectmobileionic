import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Pages} from '../interfaces/pages';
import {AuthenticateService} from '../services/auth/authentication.service';
import {Observable} from 'rxjs';
import {ShareService} from '../services/shared/share.service';


@Component({
    selector: 'app-root',
    templateUrl: 'dashboard.html',
    styleUrls: ['./dashboard.scss']
})
export class DashboardCompoment {
    response = '';
    msg$: Observable<string>;


    public appPages: Array<Pages>;
private  user:any;
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController,
        private auth: AuthenticateService ,
        private shared:ShareService,
    ) {
       



let uid=this.auth.userId();
        this.appPages = [
            {
                title: 'Projets',
                url: '/dashboard/chef-projet/',
                direct: 'root',
                icon: 'list'
            },


            {
                title: 'Statistique',
                url: '/dashboard/chef-projet/'+uid+'/state-projet',
                direct: 'forward',
                icon: 'stats'
            },
            {
                title: 'Parametrers',
                url: '/dashboard/chef-projet/'+uid+'/settings',
                direct: 'forward',
                icon: 'construct'
            }
        ];
        this.user= JSON.parse( localStorage.getItem("role"));
        console.log("**************** dashbord menu"+this.user);

        if(this.user=="dev"){
            console.log("****** Dashbordrole of current user is "+this.user);
            this.appPages = [
                {
                    title: 'Consulter taches affecte d ',
                    url: '/dashboard/chef-projet/',
                    direct: 'root',
                    icon: 'list'
                },
    
    
                {
                    title: 'Consulter taches projets d',
                    url: '/dashboard/chef-projet/'+uid+'/state-projet',
                    direct: 'forward',
                    icon: 'stats'
                },
                {
                    title: 'Parametrers d',
                    url: '/dashboard/chef-projet/'+uid+'/settings',
                    direct: 'forward',
                    icon: 'construct'
                }
            ];

        }



        if(this.user=="client"){
            console.log("**********Dashbord of current user is "+this.user);
            this.appPages = [
                {
                    title: 'Consulter projet c ',
                    url: '/dashboard/client/',
                    direct: 'root',
                    icon: 'list'
                },
    
                {
                    title: 'Deposer cahier de charge c',
                    url: '/dashboard/client/deposerchaiercharge/',
                    direct: 'forward',
                    icon: 'construct'
                }
            ];

        }

        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        }).catch(() => {});
    }

    goToEditProgile() {
        this.navCtrl.navigateRoot('dashboard/settings');
    }

    logout() {
        localStorage.clear();
        this.navCtrl.navigateRoot("");

    }
}
