import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardCompoment} from './dashboard.compoment';
import {canActivate} from '@angular/fire/auth-guard';
const paths="";
let path= localStorage.getItem("role");

const routes: Routes = [

  { path: '',  component: DashboardCompoment,
  children: [


    {
      path: 'chef-projet' ,
      loadChildren: () => import('./projets/chefProjet/projets.module')
          .then(m => m.ProjetsModule),
    },
      {
          path: 'client' ,
          loadChildren: () => import('./projets/client/client.module')
              .then(m => m.ClientModule),
      },
      {
        path:'dev',
        loadChildren: () => import('./projets/dev/dev.module')
        .then(m => m.DevModule),
        
      }
      
    
      ]}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
