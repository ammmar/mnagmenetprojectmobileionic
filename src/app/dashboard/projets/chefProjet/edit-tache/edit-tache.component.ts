import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';

@Component({
  selector: 'app-edit-tache',
  templateUrl: './edit-tache.component.html',
  styleUrls: ['./edit-tache.component.scss']
})
export class EditTacheComponent implements OnInit {
    private editTache : FormGroup;
    private id: any;
    private tacheId: any;

    constructor( private formBuilder: FormBuilder,private route:ActivatedRoute,private service:ChefProjetService) {
        this.id = this.route.snapshot.params.ids;
this.tacheId=this.route.snapshot.params.id;


        this.editTache = this.formBuilder.group({
            nameTache: ['', Validators.required],
            description: [''],


        });
    }
    logForm(){
        console
            .log("hi");
        this.service.updateTache(this.id,this.tacheId,this.editTache.value);
    }


    ngOnInit() {
        this.service.getTacheById(this.id,this.tacheId).once("value")
            .then(snapshot =>{
                let res=snapshot.val();
            this.editTache.setValue({
                nameTache: !!res.nameTache?res.nameTache:'',
                description:!!res.description?res.description:'',
            });


console.log(res)
        })

    }

}
