import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { NavController } from '@ionic/angular';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {ShareService} from '../../../../services/shared/share.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    private user: any;
    updateUser: FormGroup;
    updatePassword: FormGroup;
    submit: any;

  constructor(public navCtrl: NavController ,private shared:ShareService,private formBuilder:FormBuilder,private auth: AuthenticateService,private service:ChefProjetService) {
      this.user= JSON.parse( localStorage.getItem("currentUser"));

      this.updateUser = this.formBuilder.group({
          fullName: ['', Validators.required],
          email: ['', Validators.required],
      });
      this.updatePassword =this.formBuilder.group({
          password:['',Validators.required],

      });

  }

  ngOnInit() {
this.updateUser.setValue({
    fullName: !!this.user.fullName?this.user.fullName:"",

    email:  !!this.user.email?this.user.email:"",


  })
  }

  editProfile() {
    this.navCtrl.navigateForward('settings');
  }

  updateProfile(){

      this.service.updateProfile(this.auth.userId(),this.updateUser.value).then(success=>{

          let user= JSON.parse(localStorage.getItem("currentUser"));
user.fullName=this.updateUser.controls["fullName"].value;
          user.email=this.updateUser.controls["email"].value;
          localStorage.setItem("currentUser",JSON.stringify(user));
this.shared.setResponse(this.updateUser.value)
          Swal.fire({
              title: 'update  profile ',
              text: 'success',
              icon: 'success',
          });

      },error=>{
          Swal.fire({
              title: 'update  profile ',
              text: 'failed',
              icon: 'error',
          });
      } )
  }
  logout() {
    this.auth.logout();
    this.navCtrl.navigateRoot('/');
  }



    updatePass() {
this.auth.updatePassword(this.updateUser.controls["email"].value).then(success=>{
    Swal.fire({
        title: 'check you email ',
        text: '',
        icon: 'success',
    })},error=>{ Swal.fire({
    title: 'update  password ',
    text: 'failed',
    icon: 'error',
})}).catch(error=>{  Swal.fire({
    title: 'update  profile ',
    text: 'failed',
    icon: 'error',
})});

    }
}
