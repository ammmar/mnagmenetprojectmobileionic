import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StateProjectComponent} from './state-project.component';
import {IonicModule} from '@ionic/angular';
const routes: Routes = [
    {
        path: '',
        component: StateProjectComponent

    }
];

@NgModule({
  declarations: [StateProjectComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        IonicModule
    ]
})
export class StateProjectModule { }
