import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateProjectComponent } from './state-project.component';

describe('StateProjectComponent', () => {
  let component: StateProjectComponent;
  let fixture: ComponentFixture<StateProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
