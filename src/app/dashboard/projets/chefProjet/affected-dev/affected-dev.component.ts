import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-affected-dev',
  templateUrl: './affected-dev.component.html',
  styleUrls: ['./affected-dev.component.scss']
})
export class AffectedDevComponent implements OnInit {
    panelOpenState: boolean;
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    auto: any;
    private id: any;
    private taches: any;
private list:any;
    private devs: any[];
    private tabs= [];
    private p=[];
    constructor(private service:ChefProjetService,private route:ActivatedRoute,private router:Router) {
        this.id = this.route.snapshot;
console.log(   ((this.route.snapshot)))
        this.list=(JSON.parse(localStorage.getItem("dev-tache")));


Object.values(this.list).map(item=>{
    this.service.getDevById(item).pipe().subscribe((res)=>{
        this.p=[]
res.forEach(item=>{

         this.p.push({[item.key]:item.payload.val()});


})
        let object = Object.assign({}, ...this.p);


        this.tabs.push(object)
    });

});
console.log(this.tabs);

this.service.getTache(this.id).subscribe(res=>{
            this.taches=(JSON.parse(JSON.stringify(res)));

        })
    }


    ngOnInit() {

    }
}
