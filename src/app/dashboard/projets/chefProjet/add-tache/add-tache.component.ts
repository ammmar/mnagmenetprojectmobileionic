import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import Swal from "sweetalert2";
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-add-tache',
  templateUrl: './add-tache.component.html',
  styleUrls: ['./add-tache.component.scss']
})
export class AddTacheComponent implements OnInit {
    private editProject : FormGroup;
    private id: any;

    constructor( private nav:NavController,private formBuilder: FormBuilder,private route:ActivatedRoute,private service:ChefProjetService) {
        this.id = this.route.snapshot.params.id;

        this.editProject = this.formBuilder.group({
            nameTache: ['', Validators.required],
            description: [''],


        });
    }
    logForm(){

this.service.addTache(this.id,this.editProject.value).then(()=>{



            Swal.fire({
                title: 'add',
                text: 'sucess!',
                icon: 'success',
            })
            this.nav.back();
        });
    }


    ngOnInit() {
    }

}
