import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectTacheComponent } from './affect-tache.component';

describe('AffectTacheComponent', () => {
  let component: AffectTacheComponent;
  let fixture: ComponentFixture<AffectTacheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectTacheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectTacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
