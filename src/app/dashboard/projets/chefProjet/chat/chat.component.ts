import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
    messagesList: any[];
    newmessage:any
    private id: any;
    private idRoom: any;
    private username: string;
    private roomName:string;

  constructor(private service:ChefProjetService,private auth:AuthenticateService,private route:ActivatedRoute) {

      this.id = this.route.snapshot.params.id;
      this.idRoom=this.route.snapshot.params.ids;
      this.username=this.auth.getUserName().email;
this.service.getMessageChatRooms(this.id,this.idRoom).subscribe(response=>{
    this.messagesList=response;
})
  }

  ngOnInit() {
  }

    send() {
      console.log(this.newmessage);
        this.service.addChatMessage(this.id,this.idRoom,{
            message:this.newmessage,
            name:this.auth.getUserName()
        });
        this.newmessage='';

    }

    addEmoji($event: any) {
        
    }
}
