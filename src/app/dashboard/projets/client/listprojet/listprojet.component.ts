import { Component, OnInit } from '@angular/core';
import {AuthenticateService} from '../../../../services/auth/authentication.service';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';

import {FormControl} from '@angular/forms';
import {AlertController} from '@ionic/angular';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';
import { ClientService} from '../../../../services/client/client.service';

@Component({
  selector: 'app-listprojet',
  templateUrl: './listprojet.component.html',
  styleUrls: ['./listprojet.component.scss']
})
export class ListprojetComponent implements OnInit {

  panelOpenState: boolean;
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  auto: any;
   projects=[];
  private search: any;
  private length: any;
    taches=[];
constructor(public alertController: AlertController,private serviceclient:ClientService,private auth:AuthenticateService) {
    /*this.service.getProject(this.auth.userId()).snapshotChanges().pipe().subscribe( res=>{
        this.projects=(JSON.parse(JSON.stringify(res)));
this.search=this.projects;
this.length=this.projects.length;
    });*/

    this.serviceclient.getProjectByClientId(auth.userId()).snapshotChanges().pipe().subscribe(res=>{
        console.log("client service:"+JSON.stringify(res));
        this.projects=(JSON.parse(JSON.stringify(res)));
        this.search=this.projects;
        this.length=this.projects.length;

    });





}
bindData(data){
    this.taches=[""]
    //console.log(data.payload.taches);  
    /*if(Object.values(data.payload.taches)===null)
    {
        console.log("aucune taches a cette projets")
    }
    else{
        this.taches=Object.values(data.payload.taches);
        console.log(this.taches);
  
    }*/

    if (typeof data.payload.taches ==="undefined")
    {
    console.log('undefined');
    }
    else
    {
    console.log('defined');
    this.taches=Object.values(data.payload.taches);
    console.log(this.taches);
  
    }
}



  filterItem(){
      if(!this.myControl.value){
          this.projects=this.search;
      } // when nothing has typed
      // @ts-ignore
      this.projects = Object.assign([], this.search).filter(
          item => item.payload.nameProject.toLowerCase().indexOf(this.myControl.value.toLowerCase()) > -1
      )
  }
ngOnInit() {
}
delete(id){
   /* this.service.deleteProjet(id).then(success=>{
        Swal.fire({
            title: 'delete',
            text: ' Success!',
            icon: 'success',
        })
    })*/
}
  async alert(id) {
      const alert = await this.alertController.create({
          header: 'delete',
          message: 'This is an alert message.',
          buttons: [
              {
                  text: 'Cancel',
                  role: 'cancel',
                  cssClass: 'secondary',
                  handler: (res) => {

                  }
              }, {
                  text: 'Okay',
                  handler: () => {

this.delete(id)                    }
              }
          ]
      });


      await alert.present();
  }





}
