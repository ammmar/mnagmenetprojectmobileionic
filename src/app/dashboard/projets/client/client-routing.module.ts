import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListprojetComponent } from './listprojet/listprojet.component';
import { ListtachesComponent } from './listtaches/listtaches.component';
import { DesposerChaierChargeComponent } from './desposer-chaier-charge/desposer-chaier-charge.component';






const routes: Routes = [
      {
        path:"",
        component:ListprojetComponent
        
      },
      {
        path:"deposerchaiercharge",
        component:DesposerChaierChargeComponent
        
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
