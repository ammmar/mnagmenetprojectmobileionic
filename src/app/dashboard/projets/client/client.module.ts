import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { ListprojetComponent } from './listprojet/listprojet.component';
import { ListtachesComponent } from './listtaches/listtaches.component';
import { DesposerChaierChargeComponent } from './desposer-chaier-charge/desposer-chaier-charge.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AvatarModule} from 'ngx-avatar';

import {AngularFireModule} from '@angular/fire';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule
} from '@angular/material';


import {IonicModule} from '@ionic/angular';
import {TooltipsModule} from 'ionic-tooltips';

import {PickerModule} from '@ctrl/ngx-emoji-mart';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';
import { ChefProjetService } from '../../../services/chefprojet/chef-projet.service';
import { AuthenticateService } from '../../../services/auth/authentication.service';
import { ClientService } from '../../../services/client/client.service';




@NgModule({
  declarations: [ClientComponent, ListprojetComponent, ListtachesComponent, DesposerChaierChargeComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule, 
    MatInputModule,
    IonicModule,
    TooltipsModule,
    PickerModule,
    AvatarModule,
    FormsModule, 
    ReactiveFormsModule,
    SweetAlert2Module,
    AngularFireModule
  ],
  providers:[ChefProjetService,AuthenticateService,ClientService]
})
//RouterModule.forRoot(ClientRoutingModule.routes),
export class ClientModule { }
