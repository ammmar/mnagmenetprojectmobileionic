import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesposerChaierChargeComponent } from './desposer-chaier-charge.component';

describe('DesposerChaierChargeComponent', () => {
  let component: DesposerChaierChargeComponent;
  let fixture: ComponentFixture<DesposerChaierChargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesposerChaierChargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesposerChaierChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
