import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ClientService } from '../../../../services/client/client.service';
import { AuthenticateService } from '../../../../services/auth/authentication.service';

import {  UploadServiceService } from '../../../../services/client/upload-service.service';
import {  Upload } from '../../../../services/client/upload';

@Component({
  selector: 'app-desposer-chaier-charge',
  templateUrl: './desposer-chaier-charge.component.html',
  styleUrls: ['./desposer-chaier-charge.component.scss']
})
export class DesposerChaierChargeComponent implements OnInit {
  todo = {}
  projets:[];
  currentUpload: Upload;
  bloab:any;
  private addCahierCharge : FormGroup;
  constructor(private formBuilder: FormBuilder,private clientService:ClientService,private auth:AuthenticateService,private uplodService:UploadServiceService) { 
    this.addCahierCharge=this.formBuilder.group({
      upresume_one:['',Validators.required],
      projet:['',Validators.required]
    });


    this.clientService.getProjectByClientId(auth.userId()).snapshotChanges().pipe().subscribe(res=>{
      console.log("deposer Comonents service:"+JSON.stringify(res));
      this.projets=(JSON.parse(JSON.stringify(res)));
  

  });


  }

  ngOnInit() {
  }


  logForm() {
    let file=this.addCahierCharge.controls["upresume_one"].value;
    let projetid=this.addCahierCharge.controls["projet"].value;
    
    console.log(file);
    console.log(projetid);
 
    this.currentUpload=new Upload(file);

    //console.log("typeOf"+typeof file);

   
    this.uplodService.pushUpload(this.currentUpload,projetid);

    console.log("success uplod file");
    




   


  }

}
