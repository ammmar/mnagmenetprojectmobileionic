import { Component, OnInit } from '@angular/core';

import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ChefProjetService} from '../../../../services/chefprojet/chef-projet.service';
import {ClientService} from '../../../../services/client/client.service';
import {AuthenticateService} from '../../../../services/auth/authentication.service';


@Component({
  selector: 'app-listtaches',
  templateUrl: './listtaches.component.html',
  styleUrls: ['./listtaches.component.scss']
})
export class ListtachesComponent implements OnInit {
  panelOpenState: boolean;
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  auto: any;
  private id: any;
  private taches: [];
  isaffected: any;
  projects:[];
  
  length:any;
  private search: any;

  constructor(private serviceclient:ClientService,private auth:AuthenticateService,private route:ActivatedRoute,private router:Router) {
      this.id = this.route.snapshot.params.id;
      this.serviceclient.getProjectByClientId(auth.userId()).snapshotChanges().pipe().subscribe(res=>{
        console.log("client service Tache Componet:"+JSON.stringify(res));
        this.projects=(JSON.parse(JSON.stringify(res)));
        this.search=this.projects;
        this.length=this.projects.length;

        


    });

  }
listtacheByProjectid(id)
{


}

  listDev(key,val:any)
  {


localStorage.setItem("dev-tache",JSON.stringify(val));

  this.router.navigate(["./"+key+"/affected-dev"],{ relativeTo: this.route });
  }

  ngOnInit() {

  }

}
