import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsulterTacheAffecteComponent } from './consulter-tache-affecte.component';

describe('ConsulterTacheAffecteComponent', () => {
  let component: ConsulterTacheAffecteComponent;
  let fixture: ComponentFixture<ConsulterTacheAffecteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsulterTacheAffecteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsulterTacheAffecteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
