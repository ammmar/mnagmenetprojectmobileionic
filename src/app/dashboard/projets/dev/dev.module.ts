import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevRoutingModule } from './dev-routing.module';
import { ConsulterTacheAffecteComponent } from './consulter-tache-affecte/consulter-tache-affecte.component';
import { ConsulterTacheProjetComponent } from './consulter-tache-projet/consulter-tache-projet.component';
import { ConsulterReunionComponent } from './consulter-reunion/consulter-reunion.component';

import {AvatarModule} from 'ngx-avatar';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule
} from '@angular/material';


import {IonicModule} from '@ionic/angular';
import {TooltipsModule} from 'ionic-tooltips';

import {PickerModule} from '@ctrl/ngx-emoji-mart';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';



@NgModule({
  declarations: [ConsulterTacheAffecteComponent, ConsulterTacheProjetComponent, ConsulterReunionComponent],
  imports: [
    CommonModule,
    DevRoutingModule,
    MatAutocompleteModule,
    MatButtonModule,
    DevRoutingModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule, 
    MatInputModule,
    IonicModule,
    TooltipsModule,
    PickerModule,
    SweetAlert2Module,
    AvatarModule
  ],providers:[]
})
export class DevModule { }
