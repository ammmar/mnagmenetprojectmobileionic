import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsulterTacheProjetComponent } from './consulter-tache-projet.component';

describe('ConsulterTacheProjetComponent', () => {
  let component: ConsulterTacheProjetComponent;
  let fixture: ComponentFixture<ConsulterTacheProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsulterTacheProjetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsulterTacheProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
