import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsulterTacheAffecteComponent } from './consulter-tache-affecte/consulter-tache-affecte.component';
import { ConsulterTacheProjetComponent } from './consulter-tache-projet/consulter-tache-projet.component';
import { ConsulterReunionComponent } from './consulter-reunion/consulter-reunion.component';

const routes: Routes = [

    {
      path:"",
      component:ConsulterTacheProjetComponent
      
    },
    {
      path:"listreunion",
      component:ConsulterReunionComponent
     },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevRoutingModule { }
