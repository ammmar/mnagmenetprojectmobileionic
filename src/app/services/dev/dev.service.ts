import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
@Injectable({
  providedIn: 'root'
})
export class DevService {

  constructor(private db:AngularFireDatabase) {

   }

   public getDevById(id){
    return  this.db.list('users/dev/'+id).snapshotChanges();

    
    }

    public getprojectsById(id)
{
  //return   this.db.list('projects', ref => ref.orderByChild("dev").ref.orderByKey().equalTo("-LwhQ6VzstBMzek4Lo2x")).valueChanges()
  return  this.db.list('projects/client/'+id).snapshotChanges();

}





}
