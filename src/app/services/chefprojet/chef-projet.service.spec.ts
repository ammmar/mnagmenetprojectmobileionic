import { TestBed } from '@angular/core/testing';

import { ChefProjetService } from './chef-projet.service';

describe('ChefProjetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChefProjetService = TestBed.get(ChefProjetService);
    expect(service).toBeTruthy();
  });
});
