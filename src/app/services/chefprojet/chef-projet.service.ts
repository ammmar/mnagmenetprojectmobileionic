import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ChefProjetService {

  constructor(private db:AngularFireDatabase) { }

  public getProject(value){
    return   this.db.list('projects', ref => ref.orderByChild("chefprojet").equalTo(value))


  }
public createProject(value){
      return     this.db.list("projects").push(value)

  }
public getDevByIds(id){

      this.db.list('users/dev/'+id).snapshotChanges();
}
  public affectTache(id,tacheId,value:any){
   return    this.db.database.ref("projects/"+id+"/taches/"+tacheId).update(value)
  }

  public  getTacheById(id,tacheId){
      return this.db.database.ref("projects/"+id+"/taches/").child(tacheId);

  }
  public getProjectById(){
      return this.db.database.ref('projects');
  }
public getTache(id){
     return  this.db.list('projects/'+id+'/taches').snapshotChanges();
}
    public getAllReunion(id){
        return  this.db.list('projects/'+id+'/reunion').snapshotChanges();
    }

    public getAllChatRoom(id){
        return  this.db.list('projects/'+id+'/chatRooms').snapshotChanges();
    }
    public getMessageChatRooms(id,idRoom){
   return    this.db.list('projects/'+id+"/chatRooms/"+idRoom+'/messages').valueChanges()
    }
    public addChatMessage(id,idRoom,value)
    {
        return this.db.database.ref('projects/'+id+'/chatRooms/'+idRoom+'/messages').push(value)
    }
  public addTache(id,value){
    return   this.db.database.ref('projects/'+id+'/taches').push(value);
  }
    public updateTache(id,tacheId,value){
        this.db.database.ref('projects/'+id+'/taches/'+tacheId).update(value);
    }
    public updateProjet(id,value){
       return  this.db.database.ref('projects/'+id).update(value);
    }
    public deleteProjet(id){
        return this.db.database.ref('projects/'+id).remove();
    }


    public  addReunion(id,data){
        this.db.database.ref('projects/'+id+'/reunion').push(data);

    }

public  getAllDev(){

      return this.db.list('users/dev').snapshotChanges();
}

public getDevById(id){
      return this.db.list('users/dev/'+id).snapshotChanges();

}
    public  getAllClients(){

        return this.db.list('users/clients').snapshotChanges();
    }
    public addChatRoom(id,value){
     return    this.db.database.ref('projects/'+id+'/chatRooms').push(value);

    }
    public getChefProjectById(id){
        return  this.db.list('users/chefprojet/'+id).snapshotChanges()

    }
    public updateProfile(id,value){
       return  this.db.database.ref('users/chefprojet/'+id).update(value);

    }


}
