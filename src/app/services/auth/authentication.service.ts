import {Injectable, Provider} from '@angular/core';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {BehaviorSubject, Observable} from 'rxjs';
import {root} from 'rxjs/internal-compatibility';
import {Storage} from '@ionic/storage';
import {AngularFireDatabase} from '@angular/fire/database';
import {ChefProjetService} from '../chefprojet/chef-projet.service';
import { DevService } from '../dev/dev.service';
import { ClientService } from '../client/client.service';


@Injectable({providedIn: "root"})
export class AuthenticateService {

  public user: Observable<firebase.User>;
  private _userDetails: firebase.User = null;

private    authState = new BehaviorSubject(false);

  constructor(private _firebaseAuth: AngularFireAuth,private service:ChefProjetService,private storage: Storage,    private db: AngularFireDatabase,
  private servicedev:DevService,private Serviceclients:ClientService) {

      this.user = _firebaseAuth.authState;
      this.user.subscribe(
          (user) => {
              if (user) {
                  this._userDetails = user;
                  console.log(this._userDetails);
                  console.log("user is connected"+user)
              }
              else {
                  this._userDetails = null;
                  console.log("problem error user connection")
              }
          }
      );


  }
    get currentUserObservable(): any {
        return this._firebaseAuth.auth
    }

    get authenticated(): boolean {
        return this.authState !== null;
    }


    registerUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
          .then(

              res =>{ resolve(res);

             this._firebaseAuth.auth.currentUser.updateProfile({displayName:value.fullName});
                  if(value.role.toString().toLowerCase()==="dev") {
                      this.db.database.ref('users/dev/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }else   if(value.role.toString().toLowerCase()==="chef") {
                      this.db.database.ref('users/chefprojet/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }
                  else   if(value.role.toString().toLowerCase()==="client") {

                      this.db.database.ref('users/clients/'+this._firebaseAuth.auth.currentUser.uid).set({...value,...this._firebaseAuth.auth.currentUser.toJSON()});
                  }

                  localStorage.setItem('currentUser', JSON.stringify({...value,...this._firebaseAuth.auth.currentUser.toJSON()}));
                  console.log("RegisterInfo:"+value);
                  localStorage.setItem("role",JSON.stringify(value.role.toString()));


              },
              err => reject(err));
    });
  }

  loginUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
          .then(
              (res:any) => {resolve(res)

                console.log("********loginuserService****** the value is ==>"+value.role);

                if(value.role==="chef"){

                  let data= this.service.getChefProjectById(this._userDetails.uid).subscribe(resu=>{


                    console.log("LoginUserService:chef:==>"+JSON.parse(JSON.stringify(res))[12].payload);

                  if(value.role!==JSON.parse(JSON.stringify(resu))[12].payload){
                     this.authState.next(false);
                     
                     this.logout();
                   }
                  });
                }
                else if(value.role==="dev"){
//                  console.log("LoginUserService:dev:==>"+JSON.parse(JSON.stringify(res))[12].payload);

                this.servicedev.getDevById(this._userDetails.uid).subscribe(resu=>{


                    /*if(value.role!==JSON.parse(JSON.stringify(resu))[12].payload){
                      this.authState.next(false);
                      this.logout();
                    }*/
                    console.log("LoginUserService:dev:==>"+JSON.parse(JSON.stringify(res.role)));

                   });




                }
                else if(value==="Client")
                {
                  console.log("LoginUserService:client:==>"+JSON.parse(JSON.stringify(res))[12].payload);

                  this.Serviceclients.getclientById(this._userDetails.uid).subscribe(res=>{
                    if(value.role!==JSON.parse(JSON.stringify(res))[12].payload){
                      console.log("auth:Client:Problem  auth ");
                      this.authState.next(false);
                      this.logout();
                    }
                  });
                }
                //  console.log("Login:user id is "+this._userDetails.uid);
                //  console.log("info courant of this user  ",JSON.stringify(data));
                 // console.log("role user  ",JSON.stringify(res.user.role));
                  localStorage.setItem('currentUser', JSON.stringify(res.user));
                  localStorage.setItem("role",JSON.stringify(value.role.toString()));


              },
              
              err => reject(err))
              console.log("erreur de conexion ",value.email);
    });
  }

  logoutUser() {
    this.user=null;
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        firebase.auth().signOut()
            .then(() => {
              console.log('LOG Out');
              resolve();
            }).catch((error) => {
          reject();
        });
      }
    });
  }

    isLoggedIn() {
      if(localStorage.getItem('currentUser'))
      {
          this.authState.next(true);
      }
            this.storage.get('currentUser').then((response) => {
                if (response) {
                    this.authState.next(true);
                }
            });


    }
    isAuthenticated() {
        return this.authState.value;
    }

    logout() {
      localStorage.clear();
      this._firebaseAuth.auth.signOut();


    }
    getUserName(){
    return  JSON.parse( localStorage.getItem("currentUser"))
    }
    updatePassword(value){
      return this._firebaseAuth.auth.sendPasswordResetEmail(value);
    }

userId()
{
    let uid=JSON.parse( localStorage.getItem('currentUser'));


    return uid.uid;
}



  /* let data= this.service.getChefProjectById(this._userDetails.uid).subscribe(resu=>{


                    console.log(JSON.parse(JSON.stringify(resu))[12].payload)


                  });*/

}
