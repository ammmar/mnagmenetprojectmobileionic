import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShareService {



    msg$ = new Subject<any>();

    constructor() { }

    setResponse(data: any) {
        this.msg$.next(data);
    }

    getResponse() {
        return this.msg$;
    }

}
