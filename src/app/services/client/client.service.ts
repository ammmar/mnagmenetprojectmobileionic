import {Injectable, Provider} from '@angular/core';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {BehaviorSubject, Observable} from 'rxjs';
import {root} from 'rxjs/internal-compatibility';
import {Storage} from '@ionic/storage';
import {AngularFireDatabase} from '@angular/fire/database';
import {ChefProjetService} from '../chefprojet/chef-projet.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
 
  constructor(private db:AngularFireDatabase) { }

  public getclientById(id){
    return  this.db.list('users/clients/'+id).snapshotChanges();

    }

    public getTache(id){
      return  this.db.list('projects/'+id+'/taches').snapshotChanges();
 }


    public getProjectByClientId(id){
   // return  this.db.list('projects/client/'+id).snapshotChanges();  
   return   this.db.list('projects', ref => ref.orderByChild("client").equalTo(id))
    //return this.db.database.ref("projects/"+id+"/taches/").child(tacheId);
  
    }

    public updateProjet(id,value){
      return  this.db.database.ref('projects/'+id).update(value);
   }
}
