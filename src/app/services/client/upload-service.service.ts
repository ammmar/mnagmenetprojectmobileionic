import {Injectable, Provider} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase/app';
import {AngularFireModule} from '@angular/fire';
import { AngularFireList } from 'angularfire2/database';
import{Upload}  from './upload';

@Injectable({
  providedIn: 'root'
})
export class UploadServiceService {
  private basePath:string = '/projects';
  uploads: AngularFireList<Upload[]>;
  constructor(private db:AngularFireDatabase) { 

  }
    // Writes the file details to the realtime db

  pushUpload(upload: Upload,id) {
    let storageRef = firebase.storage().ref();
    
    let uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      },
      (error) => {
        // upload failed
        console.log(error)
      },
      () => {
        // upload success
        upload.url = uploadTask.snapshot.downloadURL
        upload.name = upload.file.name
        this.saveFileData(upload,id)

       
      }
    );



  }

    // Writes the file details to the realtime db
    private saveFileData(upload: Upload,id) {
      this.db.list(`${this.basePath}/`).push(upload);
    }




  


}
