import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {AuthenticateService} from '../../services/auth/authentication.service';

@Component({
  selector: 'popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss']
})
export class PopmenuComponent implements OnInit {
  openMenu: Boolean = false;

  constructor(public navCtrl: NavController, private  auth: AuthenticateService) {
console.log(this.auth.user)
  }

  ngOnInit() {
  }

  togglePopupMenu() {
    return this.openMenu = !this.openMenu;
  }

}
