import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from './auth/auth-guard.service';
import {LoginActivate} from './auth/loginActivate';

const routes: Routes = [

    { path: '',     canActivate:[LoginActivate],   loadChildren: () => import('./auth/login/login.module')
            .then(m => m.LoginPageModule),
    },

    { path: 'register',canActivate:[LoginActivate] , loadChildren: () => import('./auth/register/register.module')
            .then(m => m.RegisterPageModule),
    },
    { path: 'dashboard', canActivate:[AuthGuardService] , loadChildren: () => import('./dashboard/dashboard.module')
            .then(m => m.DashboardModule),
    },
    { path: '**', redirectTo: '' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
